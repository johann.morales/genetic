package com.gitlab.johannMorales.genetic;

import com.gitlab.johannMorales.genetic.exception.InvalidParameterValueException;
import com.gitlab.johannMorales.genetic.model.Cromosome;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class GeneticAlgorithmTest {

    private GeneticAlgorithm ga;

    @BeforeEach
    void setUp() {
        ga = GeneticAlgorithm.create();
    }

    @Test
    @DisplayName("Creation")
    void create() {
        assertEquals(ga.getIterations(), GeneticAlgorithm.DEFAULT_ITERATIONS);
        assertEquals(ga.getPopulationSize(), GeneticAlgorithm.DEFAULT_POPULATION_SIZE);
        assertEquals(ga.getRatioMutation(), GeneticAlgorithm.DEFAULT_RATIO_MUTATION);
        assertEquals(ga.getRatioRecombination(), GeneticAlgorithm.DEFAULT_RATIO_RECOMBINATION);
    }

    @Test
    @DisplayName("Set iterations")
    void withIterations() {
        int newValue = 200;
        ga.withIterations(newValue);
        assertEquals(ga.getIterations(), newValue);
    }

    @Test
    @DisplayName("Set iterations fails if bad value")
    void withIterationsFailsIfBadValue() {
        int zero = 0;
        int lessThanZero = -1;
        assertThrows(InvalidParameterValueException.class,()->ga.withIterations(zero));
        assertThrows(InvalidParameterValueException.class,()->ga.withIterations(lessThanZero));
    }

    @Test
    @DisplayName("Set populationSize")
    void withPopulationSize() {
        int newValue = 200;
        ga.withPopulationSize(newValue);
        assertEquals(ga.getPopulationSize(), newValue);
    }

    @Test
    @DisplayName("Set populationSize fails if bad value")
    void iwithPopulationSizeFailsIfBadValue() {
        int zero = 0;
        int lessThanZero = -1;
        assertThrows(InvalidParameterValueException.class,()->ga.withPopulationSize(zero));
        assertThrows(InvalidParameterValueException.class,()->ga.withPopulationSize(lessThanZero));
    }

    @Test
    @DisplayName("Set recombinationRatio")
    void withRecombinationRatio() {
        double newValue = 0.5;
        ga.withRecombinationRatio(newValue);
        assertEquals(ga.getRatioRecombination(), newValue);
    }

    @Test
    @DisplayName("Set recombinationRatio fails if bad value")
    void withRecombinationRatioFailsIfBadValue() {
        double moreThanOne = 1.5;
        double lessThanZero = -1.0;
        assertThrows(InvalidParameterValueException.class,()->ga.withRecombinationRatio(moreThanOne));
        assertThrows(InvalidParameterValueException.class,()->ga.withRecombinationRatio(lessThanZero));
    }


    @Test
    @DisplayName("Set mutationRatio")
    void withMutationRatio() {
        double newValue = 0.5;
        ga.withMutationRatio(newValue);
        assertEquals(ga.getRatioMutation(), newValue);
    }

    @Test
    @DisplayName("Set mutationRatio fails if bad value")
    void withMutationRatioFailsIfBadValue() {
        double moreThanOne = 1.5;
        double lessThanZero = -1.0;
        assertThrows(InvalidParameterValueException.class,()->ga.withMutationRatio(moreThanOne));
        assertThrows(InvalidParameterValueException.class,()->ga.withMutationRatio(lessThanZero));
    }


    @Test
    @DisplayName("Define factory function")
    void defineFactory() {
        ga.defineFactory(Cromosome::new);
        assertNotNull(ga.getCromosomeFactory());
    }

    @Test
    @DisplayName("Define factory function fails if null")
    void defineFactoryFailsIfNull() {
        assertThrows(java.lang.NullPointerException.class,()->ga.defineFactory(null));
    }

    @Test
    @DisplayName("Define fitness function")
    void defineFitness() {
        ga.defineFitness(c->2.0);
        assertNotNull(ga.getFitness());
    }

    @Test
    @DisplayName("Define fitness fails if null")
    void defineFitnessFailsIfNull() {
        assertThrows(java.lang.NullPointerException.class,()->ga.defineFitness(null));
    }


    @Test
    @DisplayName("Define recombination function")
    void defineRecombination() {
        ga.defineRecombination((a, b) -> a);
        assertNotNull(ga.getRecombination());
    }

    @Test
    @DisplayName("Define recombination fails if null")
    void defineRecombinationFailsIfNull() {
        assertThrows(java.lang.NullPointerException.class,()->ga.defineRecombination(null));
    }

    @Test
    @DisplayName("Define mutation function")
    void defineMutation() {
        ga.defineMutation(a->a);
        assertNotNull(ga.getMutation());
    }

    @Test
    @DisplayName("Define mutation fails if null")
    void defineMutationFailsIfNull() {
        assertThrows(java.lang.NullPointerException.class,()->ga.defineMutation(null));
    }

}