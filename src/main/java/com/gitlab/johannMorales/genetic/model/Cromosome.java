package com.gitlab.johannMorales.genetic.model;

import lombok.Getter;
import lombok.Setter;

import java.util.BitSet;

public class Cromosome {

    @Getter
    @Setter
    private BitSet data;


    public static Cromosome defaultFactory(int size) {
        Cromosome cromosome = new Cromosome();
        cromosome.setData(new BitSet(size));
        return cromosome;
    }

}
