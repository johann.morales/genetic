package com.gitlab.johannMorales.genetic;

import com.gitlab.johannMorales.genetic.exception.InvalidParameterValueException;
import com.gitlab.johannMorales.genetic.exception.MissingParameterException;
import com.gitlab.johannMorales.genetic.lambdas.CromosomeFactory;
import com.gitlab.johannMorales.genetic.lambdas.Fitness;
import com.gitlab.johannMorales.genetic.lambdas.Mutation;
import com.gitlab.johannMorales.genetic.lambdas.Recombination;
import com.gitlab.johannMorales.genetic.model.Cromosome;
import lombok.Getter;
import lombok.NonNull;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Slf4j
public class GeneticAlgorithm {

    public static final int DEFAULT_ITERATIONS = 5000;
    public static final int DEFAULT_POPULATION_SIZE = 100;
    public static final double DEFAULT_RATIO_MUTATION = 0.3;
    public static final double DEFAULT_RATIO_RECOMBINATION = 0.2;

    @Getter
    @ToString.Exclude
    private CromosomeFactory cromosomeFactory;

    @Getter
    @ToString.Exclude
    private Fitness fitness;

    @Getter
    @ToString.Exclude
    private Mutation mutation;

    @Getter
    @ToString.Exclude
    private Recombination recombination;

    @Getter
    @ToString.Include
    private int iterations;

    @Getter
    @ToString.Include
    private int populationSize;

    @Getter
    @ToString.Include
    private double ratioMutation;

    @Getter
    @ToString.Include
    private double ratioRecombination;

    private List<Cromosome> population;

    private GeneticAlgorithm(){}

    public static GeneticAlgorithm create() {
        GeneticAlgorithm ga = new GeneticAlgorithm();
        ga.cromosomeFactory = null;
        ga.fitness = null;
        ga.mutation = null;
        ga.recombination = null;
        ga.iterations = DEFAULT_ITERATIONS;
        ga.populationSize = DEFAULT_POPULATION_SIZE;
        ga.ratioMutation = DEFAULT_RATIO_MUTATION;
        ga.ratioRecombination = DEFAULT_RATIO_RECOMBINATION;
        ga.population = null;
        return ga;
    }

    public GeneticAlgorithm withIterations(final int iterations) throws InvalidParameterValueException {
        if(iterations <= 0) {
            log.error("Invalid value for iterations amount: {}", iterations);
            throw new InvalidParameterValueException();
        }
        this.iterations = iterations;
        return this;
    }

    public GeneticAlgorithm withPopulationSize(final int populationSize) throws InvalidParameterValueException {
        if(populationSize <= 0) {
            log.error("Invalid value for populationSize amount: {}", populationSize);
            throw new InvalidParameterValueException();
        }
        this.populationSize = populationSize;
        return this;
    }

    public GeneticAlgorithm withMutationRatio(final double ratio) throws InvalidParameterValueException {
        if(ratio < 0 || ratio > 1) {
            log.error("Invalid value for mutation ratio: {}", ratio);
            throw new InvalidParameterValueException();
        }
        this.ratioMutation = ratio;
        return this;
    }

    public GeneticAlgorithm withRecombinationRatio(final double ratio) throws InvalidParameterValueException {
        if(ratio < 0 || ratio > 1) {
            log.error("Invalid value for recombination ratio: {}", ratio);
            throw new InvalidParameterValueException();
        }
        this.ratioRecombination = ratio;
        return this;
    }

    public GeneticAlgorithm defineFactory(@NonNull final CromosomeFactory cromosomeFactory) {
        this.cromosomeFactory = cromosomeFactory;
        return this;
    }

    public GeneticAlgorithm defineRecombination(@NonNull final Recombination recombination) {
        this.recombination = recombination;
        return this;
    }

    public GeneticAlgorithm defineMutation(@NonNull final Mutation mutation) {
        this.mutation = mutation;
        return this;
    }

    public GeneticAlgorithm defineFitness(@NonNull final Fitness fitness) {
        this.fitness = fitness;
        return this;
    }


    public Cromosome run() throws MissingParameterException {
       checkRunParameters();
       initPopulation();
        List<Integer> range = IntStream.range(0, populationSize).boxed()
                .collect(Collectors.toCollection(ArrayList::new));

        for (int iteration = 1; iteration <= this.iterations; iteration++) {
            if(this.recombination != null) {
                Collections.shuffle(range);
                for (int i = 0; i < populationSize*ratioRecombination; i++) {
                    population.add(recombination.recombine(population.get(i), population.get(populationSize-i-1)));
                }
            }

            if(this.mutation != null) {
                Collections.shuffle(range);
                for (int i = 0; i < populationSize*ratioMutation; i++) {
                    population.add(mutation.mutate(population.get(i)));
                }
            }

            sortPopulation();
            population = population.subList(0, populationSize);
        }

        return population.get(0);
    }

    private void checkRunParameters() throws MissingParameterException {
        if(this.cromosomeFactory == null) {
            log.error("Cromosome factory is not defined");
            throw new MissingParameterException();
        }

        if(this.fitness == null) {
            log.error("Fitness is not defined");
            throw new MissingParameterException();
        }

        if(this.recombination == null) {
            log.warn("Recombination is not set, skipping...");
        }

        if(this.mutation == null) {
            log.warn("Mutation is not set, skipping...");
        }
    }

    private void initPopulation(){
        for (Integer i = 0; i < this.populationSize; i++) {
            this.population.add(cromosomeFactory.create());
        }
        sortPopulation();
    }

    private void sortPopulation() {
        population.sort(Comparator.comparing(c-> fitness.calculate((Cromosome) c)).reversed());
    }

}
