package com.gitlab.johannMorales.genetic;

import com.gitlab.johannMorales.genetic.model.Cromosome;

public interface GeneticProblem {

    Cromosome cromosomeFactory();

    Double fitness();

    Cromosome mutation(Cromosome cromosome);

    Cromosome recombination(Cromosome cromosomeA, Cromosome cromosomeB);

}
