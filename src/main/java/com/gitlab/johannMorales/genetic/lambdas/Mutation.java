package com.gitlab.johannMorales.genetic.lambdas;

import com.gitlab.johannMorales.genetic.model.Cromosome;

public interface Mutation {

    Cromosome mutate(final Cromosome cromosome);

}
