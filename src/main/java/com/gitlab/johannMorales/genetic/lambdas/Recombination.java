package com.gitlab.johannMorales.genetic.lambdas;

import com.gitlab.johannMorales.genetic.model.Cromosome;

public interface Recombination {

    Cromosome recombine(final Cromosome cromosomeA, final Cromosome cromosomeB);
}
