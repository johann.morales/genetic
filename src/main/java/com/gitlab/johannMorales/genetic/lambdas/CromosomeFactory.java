package com.gitlab.johannMorales.genetic.lambdas;

import com.gitlab.johannMorales.genetic.model.Cromosome;

public interface CromosomeFactory {

    Cromosome create();

}
