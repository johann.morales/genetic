package com.gitlab.johannMorales.genetic.lambdas;

import com.gitlab.johannMorales.genetic.model.Cromosome;

public interface Fitness {

    Double calculate(final Cromosome cromosome);

}
